/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 */

var map;
var tiendas = [];


function llamada_ajax(){
    $.ajax({

                    type: 'POST',
                    url: 'http://adminapp.growbarato.net/tiendasjson.php',
                    dataType: 'json', // serializes the form's elements.
                    contentType: 'application/json; charset=UTF-8', // This is the money shot
                    //data: arr,
                    success: function (data) {
                        //now json variable contains data in json format
                        //let's display a few items
                        
                        tiendas = data;
                        /*for (var i=0;i<data.length;++i)
                        {
                            
                            $('body').append('<div class="name">'+data[i].id+'--'+data[i].nombre_tienda+'</>');
                        }
                       // alert(data[0].nombre_tienda);
                        */
                    },
                    
                    error: function (data) {
//                        alert("error");
                        showAlert("al obtener la lista de tiendas en el mapa. Revise la conexión de la red", "Error");
                        
                        console.log(data);
                    }


                }).done(function() { //use this
  initMap();
});
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map_canvas'), {
    });
    
   
    
    
    
  
    var latlng_pos=[];
    var latitud;
    var longitud;
    var nombre;
    var id;
    var direccion;
    var numero;
    var poblacion;
    var provincia;
    var cp;
    var j = 0;
    var currencies = [];
    function tienda(name, cp, provincia, lat, lng){
        this.value = name + ' ' + cp + ' ' + provincia;
        this.lat = lat;
        this.lng = lng;
    }
    
    function datosTienda(id, nombre, direccion, numero, poblacion, provincia, cp, lat, lng){
        this.id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.numero = numero;
        this.poblacion = poblacion;
        this.provincia = provincia;
        this.cp = cp;
        this.lat = lat;
        this.lng = lng;
    }
    
    var image = {
    url: 'img/semilla_grow40.png',
//    // This marker is 20 pixels wide by 32 pixels high.
//    size: new google.maps.Size(32, 32),
//    // The origin for this image is (0, 0).
//    origin: new google.maps.Point(0, 0),
//    // The anchor for this image is the base of the flagpole at (0, 32).
//    anchor: new google.maps.Point(0, 32)
    };
    
    for (var i = 0; i < tiendas.length; ++i) {
        id = tiendas[i].id;
        nombre = tiendas[i].nombre_tienda;
        direccion = tiendas[i].direccion;
        numero = tiendas[i].numero;
        poblacion = tiendas[i].poblacion;
        provincia = tiendas[i].provincia;
        cp = tiendas[i].cp;
        latitud = Number(tiendas[i].latitud);
        longitud = Number(tiendas[i].longitud);
        if(latitud !== 0.0 && longitud !== 0.0){
            var insertarTienda = new tienda(nombre, cp, provincia, latitud, longitud);
            var dtienda = new datosTienda(id, nombre, direccion, numero, poblacion, provincia, cp, latitud, longitud);
            currencies.push(insertarTienda);
            latlng_pos[j]=new google.maps.LatLng(latitud,longitud);
            var marker = new google.maps.Marker({
                position: {
                    lat: latitud,
                    lng: longitud
                },
                    map: map,
                    icon: image,
//                    icon: "img/semilla_growbarato12.png",
                    
//                    icon: obtenerIcono(tiendas[i]),
                    title: nombre
                });

            attachSecretMessage(marker, nombre, dtienda);
            j++;
        }
    }
  

    $('#autocomplete').autocomplete({
        lookup: currencies,
        onSelect: function (suggestion) {
            var thehtml = '<strong>Currency Name:</strong> ' + suggestion.lat + ' <br> <strong>Symbol:</strong> ' + suggestion.data;
            $('#outputcontent').html(thehtml);
            var mapCenter = new google.maps.LatLng(suggestion.lat, suggestion.lng);

            console.log("pasa por prueba()");
            map.setZoom(16);
            map.setCenter(mapCenter);
        }
    });
        
  
    //mostrar zoom hidrante seleccionado, si existe selección.
    /*if (localStorage.getItem('latSel') !== null && localStorage.getItem('lngSel') !== null){
        var lat = localStorage.getItem('latSel');
        var lng = localStorage.getItem('lngSel');
        var mapCenter = new google.maps.LatLng(lat, lng);

        console.log("pasa por prueba()");
        map.setZoom(16);
        map.setCenter(mapCenter);

        localStorage.removeItem('latSel');
        localStorage.removeItem('lngSel');    
    }else{

        //para centrar el mapa dependiendo de las coordenadas de los hidrantes existentes
        var latlngbounds = new google.maps.LatLngBounds( );
        for ( var i = 0; i < latlng_pos.length; i++ ) {
            latlngbounds.extend( latlng_pos[ i ] );
        }
        map.fitBounds( latlngbounds );
    }
    */
   
   //para centrar el mapa dependiendo de las coordenadas de los hidrantes existentes
        var latlngbounds = new google.maps.LatLngBounds( );
        for ( var i = 0; i < latlng_pos.length; i++ ) {
            latlngbounds.extend( latlng_pos[ i ] );
        }
        map.fitBounds( latlngbounds );
  
    
    
    setTimeout(function(){
        $.mobile.loading('hide');
    },1);
  
 }
 
function obtenerIcono(h){
    var icono = "../img/semilla.png";
    return icono;
}

  
function attachSecretMessage(marker, secretMessage, tienda) {
    var infowindow = new google.maps.InfoWindow({
        //content: secretMessage
        content : "<div id='iw_container'>" +
                  "<div class='iw_title'><strong>"+secretMessage+"</strong></div>" +
                  "<div class='iw_content'>"+tienda.direccion+", nº "+tienda.numero+"</div>" +
                  "<div class='iw_content'>"+tienda.cp+", "+tienda.poblacion+" " +
                  "<a href='https://www.google.com/maps/dir//"+ tienda.lat + "," + tienda.lng + "'><img src='img/Maps-icon.png' class='ui-li-icon ui-corner-none' style='display: block; width: 23px; float: right;'>" + 
                  "</div></a>"
                 
                  
        /*content : "<div id='iw_container'>" +
                  "<div class='iw_title'><strong>"+secretMessage+"</strong></div>" + comprobarPermisoSupervision(id)*/
//                  "<div class='iw_content'><input type='button' style='margin-top: 4px;' value='Supervisar' onclick='localStorage.setItem(\"idHidranteSeleccionado\","+id+"); location.href=\"../ListaDeElementos/listaDeElementosDeHidrante.html\";'>" +
//                  "</div>"

    });

marker.addListener('click', function() {
   // analytics.trackEvent("tiendas", "consultar", tienda.nombre, tienda.id);
    infowindow.open(marker.get('map'), marker);
    
  });

var update_timeout = null;  
  
marker.addListener('click', function(){
    update_timeout = setTimeout(function(){
        //alert(tienda.nombre + ' ' + tienda.id);
        analytics.trackEvent("tiendas", "consultar", tienda.nombre, tienda.id);  
        infowindow.open(marker.get('map'), marker);
    }, 200);        
});

marker.addListener('dblclick', function() {
    clearTimeout(update_timeout);
    var mapCenter = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
    map.setZoom(16);
    map.setCenter(mapCenter);
    });
}


//  marker.addListener('click', function() {
//    infowindow.open(marker.get('map'), marker);
//  });
//
//var update_timeout = null;  
//  
//marker.addListener('click', function(){
//    update_timeout = setTimeout(function(){
//        infowindow.open(marker.get('map'), marker);
//    }, 200);        
//});
//
//marker.addListener('dblclick', function() {
//    clearTimeout(update_timeout);
//    var mapCenter = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
//    map.setZoom(16);
//    map.setCenter(mapCenter);
//    });
//}

/*function comprobarPermisoSupervision(id){
    
    var textoSms = "</div>";
    
    if (localStorage.getItem('opcionesUsuario') !== null && localStorage.getItem('opcionesUsuario') !== undefined){
        var opcionesUsuario = JSON.parse(localStorage.getItem('opcionesUsuario'));
        var tienePermisoSupervision = false;
        
        if (opcionesUsuario.length > 0){
            var x = 0;
            for (x=0; x<opcionesUsuario.length; x++){

                if (opcionesUsuario[x] === 32){ //32- SUPERVISION_HIDRANTE("Supervisión de hidrante"), //32
                    tienePermisoSupervision = true;
                }
                
            }

            if(tienePermisoSupervision){
                textoSms = "<div class='iw_content'><input type='button' style='margin-top: 4px;' value='Supervisar' onclick='localStorage.setItem(\"idHidranteSeleccionado\","+id+"); location.href=\"../ListaDeElementos/listaDeElementosDeHidrante.html\";'>" +
                           "</div>";
            }

        }

    }
    
    return textoSms;
    
}*/

/*
function tieneRiego(h) {
    if ((h === "TCH_GPRS_1DIG_0ANA")
            || (h === "TCH_GPRS_2DIG_2ANA")
            || (h === "TCH_GPRS_2DIG_0ANA")
            || (h === "TCH_GPRS_0DIG_2ANA") 
            || (h === "TCH_RADIO_MODEM_END_POINT")
            || (h === "TCH_RADIO_MODEM_REPETIDOR")) {
        return true;
    } else {
        return false;
    }
}*/

$(document).on("pageinit", "#mapa-tiendas", function() {
        //initMap();
        llamada_ajax();
    $(document).ready(function() {
        var altTitulo = $("#titulo").height();
        var altPage = $("#mapa-tiendas").height();
        var altPie = $("#pie").height();
        //$("#map_canvas").height(screen.height - altTitulo - altPie - 30 );
        $("#map_canvas").height($(window).height() - altTitulo - altPie - (scroll.length + 2));

        console.log("altScreen: " + screen.height);
        console.log("titulo: " + altTitulo);
        console.log("altPie: " + altPie);
        console.log("altPage: " + altPage);
        console.log("map_canvas: " + $("#map_canvas").height());
    });

});


//$(document).on("pageinit", "#mapa-tiendas", function() {
//    initMap();
//    $(document).ready(function() {
//        var altTitulo = $("#titulo").height();
//        var altPage = $("#mapa-tiendas").height();
//        var altPie = $("#pie").height();
////        $("#map_canvas").height(screen.height - altTitulo - altPie - 30 );
//        $("#map_canvas").height($(window).height() - altTitulo - altPie - (scroll.length + 2));
//
//        console.log("altScreen: " + screen.height);
//        console.log("titulo: " + altTitulo);
//        console.log("altPie: " + altPie);
//        console.log("altPage: " + altPage);
//        console.log("map_canvas: " + $("#map_canvas").height());
//    });
//
//});

//$(document).on("pageinit", "#mapa-tiendas", function() {
//    initMap();
//    $(document).ready(function() {
//        var altTitulo = $("#titulo").height();
//        var altPage = $("#mapa-tiendas").height();
//        var altPie = $("#pie").height();
//        $("#map_canvas").height(screen.height - altTitulo - altPie - 30 );
//        $("#map_canvas").height($(window).height() - (scroll.length + 2));
//
//        console.log("altScreen: " + screen.height);
//        console.log("altPage: " + altPage);
//        console.log("map_canvas: " + $("#map_canvas").height());
//    });
//
//});


function miFuncionDeRefresco(){
    obtenerIdSesionDeRefresco();
    initMap();
}

function showAlert(message, title) {
    if (window.navigator.notification) {
        window.navigator.notification.alert(message, null, title, 'OK');
    } else {
        alert(title ? (title + ": " + message) : message);
    }
}

function obtenerIdSesionDeRefresco(){
    
    setTimeout(function(){
        $.mobile.loading( 'show', {
            text: "Obteniendo datos...",
            textVisible: true,
            theme: "b",
            textonly: false,
            html: ""
        });
    },1);
    
    /*
    try{
        var usuarioId = $.session.get("usuarioId");
        if(usuarioId !== undefined){
            interfazMovil = new interfazmovil_isrl_web_inelcom_com__InterfazMovilWS();
            interfazMovil.url = localStorage.getItem('urlServidor');
            getHidrantesYconcentradoresDeRefresco(usuarioId);
        }else{
          location.href = "index.html";
        }
    } catch (err) {
            showAlert(err, "Error en la obtención de el id del usuario");
    }
    */
}


/*function getHidrantesYconcentradoresDeRefresco(idUser) {

    console.log("getHidrantesYconcentradores(" + idUser + ")");

    function onSuccess(result) {
        var hidrantesYconcentradores;
        hidrantesYconcentradores = result.getReturn();

        //AQUÍ GUARDAMOS OBJETO SECTORES EN LOCAL STORAGE:
        localStorage.setItem('localHidrantesYconcentradores', JSON.stringify(hidrantesYconcentradores));
        
        return true;
    }

    function onError(httpStatus, httpStatusText) {
        var errorXML =  this.req.response;
        var n1 = errorXML.indexOf("<message>");
        var n2 = errorXML.indexOf("</message>");
        var res = errorXML.substring(n1 + 9, n2);
        showAlert(res + ".", "Error del servidor en la obtención de hidrantes y concentradores del refresco");
        return false;
    }

    try {
        interfazMovil.getHidrantesYConcentradores(onSuccess, onError, idUser);
    } catch (err) {
        console.log(err);
    }

}*/


/*  MAPA POPUP  */

/*
function initialize() {

    console.log("pasa por initialize");
    
    var lat = localStorage.getItem('latSel');
    var lng = localStorage.getItem('lngSel');     
    
    try{
        var mapProp = {
            center:new google.maps.LatLng(lat,lng),
            zoom:14,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas_popup"),mapProp);


        var idSeleccionado = Number(localStorage.getItem('idHidranteSeleccionado')); 
        var latitud;
        var longitud;
        var nombre;
        var id;

        for (var i = 0; i < HidrantesYconcentradoresEnLocal.length; ++i) {
            latitud = HidrantesYconcentradoresEnLocal[i]._posY;
            longitud = HidrantesYconcentradoresEnLocal[i]._posX;
            nombre = HidrantesYconcentradoresEnLocal[i]._nombre;
            id = HidrantesYconcentradoresEnLocal[i]._id;

            if (id === idSeleccionado){
                var marker = new google.maps.Marker({
                    position: {
                        lat: latitud,
                        lng: longitud
                    },
                        map: map,
                        icon: obtenerIcono(HidrantesYconcentradoresEnLocal[i]),
                        title: nombre
                    });
                break;
            }

        }
            
        var varTiempo = setInterval(refresco, 500);

        function refresco(){
            var prueba = localStorage.getItem('bool');
            if (prueba === "true"){
                localStorage.setItem('bool', 'false');
                var mapCenter = new google.maps.LatLng(latitud, longitud);
                map.setZoom(14);
                map.setCenter(mapCenter);
                console.log("pasa per interval");
                
            } else {
                console.log("pasa clear");
                clearInterval(varTiempo);
            }
        }
    }catch(err){
        console.log("Problema en la incialización mapa: " + err);
    }
    
}*/


